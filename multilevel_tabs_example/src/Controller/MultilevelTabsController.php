<?php

namespace Drupal\multilevel_tabs_example\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses example data.
 */
class MultilevelTabsController extends ControllerBase {

  /**
   * Example tab empty page.
   *
   * @param string $message
   *   The displayed message.
   *
   * @return array
   *   The render array.
   */
  public function exampleRoute(string $message = '') {
    $message = empty($message) ? t('Coming soon...') : $message;
    return [
      '#markup' => '<span>' . $message . '</span>',
    ];
  }

}
