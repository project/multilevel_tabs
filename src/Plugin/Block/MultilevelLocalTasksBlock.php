<?php

namespace Drupal\multilevel_tabs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\multilevel_tabs\MultilevelLocalTaskManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Multilevel Tabs" block to display the local tasks.
 *
 * @Block(
 *   id = "multilevel_local_tasks_block",
 *   admin_label = @Translation("Multilevel Tabs"),
 * )
 */
class MultilevelLocalTasksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The multilevel local task manager.
   *
   * @var \Drupal\Core\Menu\LocalTaskManagerInterface
   */
  protected LocalTaskManagerInterface $localTaskManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Creates a MultilevelLocalTasksBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\LocalTaskManagerInterface $local_task_manager
   *   The multilevel local task manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LocalTaskManagerInterface $local_task_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->localTaskManager = $local_task_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('multilevel_tabs.menu.local_task'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'menu_style' => FALSE,
      'display_single' => FALSE,
      'primary' => TRUE,
      'secondary' => TRUE,
      'tertiary' => TRUE,
      'quaternary' => TRUE,
      'quinary' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->localTaskManager);
    $display_single_tab = $config['display_single'];
    $menu_style_tabs = $config['menu_style'];
    $tabs = [
      '#theme' => 'multilevel_menu_local_tasks',
      '#menu_style' => $menu_style_tabs,
    ];
    $tabs_exist = FALSE;

    foreach (MultilevelLocalTaskManager::getLevelsMap() as $index => $level) {
      if ($config[$level]) {
        $links = $this->localTaskManager->getLocalTasks($this->routeMatch->getRouteName(), $index - 1);
        $cacheability = $cacheability->merge($links['cacheability']);

        // Handle single tab display.
        $level_tabs = $links['tabs'];
        if (!$display_single_tab || $menu_style_tabs) {
          $level_tabs = count(Element::getVisibleChildren($level_tabs)) > 1 ? $level_tabs : [];
        }
        if (!empty($level_tabs)) {
          $tabs_exist = TRUE;
        }

        $tabs += [
          '#' . $level => $level_tabs,
        ];
      }
    }

    $build = [];
    $cacheability->applyTo($build);
    if (!$tabs_exist) {
      return $build;
    }

    return $build + $tabs;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;
    $form['levels'] = [
      '#type' => 'details',
      '#title' => $this->t('Shown tabs'),
      '#description' => $this->t('Select tabs being shown in the block'),
      '#open' => TRUE,
    ];
    $form['levels']['menu_style'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show tabs as nested menu'),
      '#default_value' => $config['menu_style'],
    ];
    $form['levels']['display_single'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display single tabs'),
      '#default_value' => $config['menu_style'],
    ];
    foreach (MultilevelLocalTaskManager::getLevelsMap() as $level) {
      $form['levels'][$level] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show @level tabs', ['@level' => $level]),
        '#default_value' => $config[$level],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $levels = $form_state->getValue('levels');
    $this->configuration['menu_style'] = $levels['menu_style'];
    $this->configuration['display_single'] = $levels['display_single'];
    foreach (MultilevelLocalTaskManager::getLevelsMap() as $level) {
      $this->configuration[$level] = $levels[$level];
    }
  }

}
