<?php

/**
 * @file
 * Expand menu items and set active-trail according to current path.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\multilevel_tabs\MultilevelLocalTaskManager;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_help().
 */
function multilevel_tabs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the multilevel_tabs module.
    case 'help.page.multilevel_tabs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Multilevel Tabs') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function multilevel_tabs_theme($existing, $type, $theme, $path) {
  $levels = array_values(MultilevelLocalTaskManager::getLevelsMap());
  return [
    'multilevel_menu_local_tasks' => [
      'variables' => [
        'menu_style' => NULL,
        'attributes' => [],
      ] + array_fill_keys($levels, []),
    ],
    'multilevel_menu_local_task' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_preprocess_HOOK() for multilevel-menu-local-tasks templates.
 */
function template_preprocess_multilevel_menu_local_tasks(&$variables) {
  $level_map = MultilevelLocalTaskManager::getLevelsMap();
  $variables['attributes'] = new Attribute($variables['attributes']);
  if ($variables['menu_style']) {
    $level_map = array_reverse($level_map, TRUE);
    $variables['primary']['#attached'] = ['library' => ['multilevel_tabs/tabs']];
    $last_level = FALSE;
    $nested_tabs = [];
    foreach ($level_map as $i => $level) {
      if (!empty($variables[$level]) && !$last_level) {
        $nested_tabs = $variables[$level];
        $last_level = TRUE;
      }
      elseif (!empty($variables[$level]) && $nested_tabs) {
        $nested_tabs = [
          '#theme' => 'multilevel_menu_local_tasks',
          '#menu_style' => $variables['menu_style'],
          '#primary' => $nested_tabs,
          '#attributes' => [
            'class' => !empty($level_map[$i + 1]) ? [$level_map[$i + 1]] : [],
          ],
        ];

        foreach ($variables[$level] as $plugin_id => $tab_build) {
          if (empty($tab_build['#active'])) {
            continue;
          }
          $variables[$level][$plugin_id]['#subtabs'] = $nested_tabs;
        }
        $nested_tabs = $variables[$level];
      }
      if ($level !== 'primary') {
        $variables[$level] = [];
      }
    }
  }
  else {
    foreach ($level_map as $level) {
      if (!empty($variables[$level])) {
        $variables[$level]['#attached'] = [
          'library' => [
            'multilevel_tabs/tabs',
          ],
        ];
        break;
      }
    }
  }
}

/**
 * Prepares variables for single local task link templates.
 *
 * Default template: multilevel-menu-local-task.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element containing:
 *     - #link: A menu link array with 'title', 'url', and (optionally)
 *       'localized_options' keys.
 *     - #active: A boolean indicating whether the local task is active.
 *     - #subtabs: (optional) A subtabs array for rendering.
 */
function template_preprocess_multilevel_menu_local_task(&$variables) {
  $link = $variables['element']['#link'];
  $link += [
    'localized_options' => [],
  ];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    $variables['is_active'] = TRUE;

    // Add text to indicate active tab for non-visual users.
    $active = new FormattableMarkup('<span class="visually-hidden">@label</span>', ['@label' => t('(active tab)')]);
    $link_text = t('@local-task-title@active', ['@local-task-title' => $link_text, '@active' => $active]);
  }

  $link['localized_options']['set_active_class'] = TRUE;

  $variables['link'] = [
    '#type' => 'link',
    '#title' => $link_text,
    '#url' => $link['url'],
    '#options' => $link['localized_options'],
  ];

  $variables['link']['#options']['attributes']['class'][] = 'multilevel-tabs__link';

  if (!empty($variables['element']['#subtabs'])) {
    $variables['subtabs'] = $variables['element']['#subtabs'];
  }
}
